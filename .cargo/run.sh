#!/usr/bin/env bash
trap 'trap - SIGINT && kill -SIGINT 0' SIGINT SIGTERM EXIT
shopt -s extglob
real="$(find "$(dirname "$1")"/deps -samefile "$1" -print -quit)"
mono ~/Mesen-X/bin/x64/Release/Mesen.exe "$1" .cargo/debug_log.lua 3> >(defmt-print -e "$real.elf")
