﻿output = io.open("/dev/fd/3", "wb")

DEBUG_WRITE = 0x4009
DEBUG_FLUSH = 0x400D
DEBUG_BREAK = 0x4018
DEBUG_INIT = 0x4019

ready = false
frames = 0

function onWrite(address, value)
  if ready then
    output:write(string.char(value))
  end
end

function onFlush(address, value)
  output:flush()
end

function onBreak(address, value)
  if ready then
    emu.breakExecution()
  end
end

function onInit(address, value)
  emu.log('init')
  ready = true
end

function onReset()
  emu.log('reset')
  ready = false
  frames = 0
end

function onFrame()
  if not ready then
    frames = frames + 1
    if frames > 5 then
      emu.reset()
      frames = 0
    end
  end
end

emu.addMemoryCallback(onWrite, emu.memCallbackType.cpuWrite, DEBUG_WRITE)
emu.addMemoryCallback(onFlush, emu.memCallbackType.cpuWrite, DEBUG_FLUSH)
emu.addMemoryCallback(onBreak, emu.memCallbackType.cpuWrite, DEBUG_BREAK)
emu.addMemoryCallback(onInit, emu.memCallbackType.cpuWrite, DEBUG_INIT)
emu.addEventCallback(onReset, emu.eventType.reset)
emu.addEventCallback(onFrame, emu.eventType.endFrame)
