use crate::regs::cpu::{DEBUG_BREAK, DEBUG_FLUSH, DEBUG_INIT, DEBUG_WRITE};

#[defmt::global_logger]
struct Logger;

unsafe impl defmt::Logger for Logger {
    fn acquire() {}
    unsafe fn release() {}

    unsafe fn write(bytes: &[u8]) {
        for &byte in bytes {
            DEBUG_WRITE.write(byte);
        }
        DEBUG_FLUSH.write(0);
    }

    unsafe fn flush() {
        DEBUG_FLUSH.write(0);
    }
}

#[defmt::panic_handler]
#[allow(clippy::empty_loop)]
fn defmt_panic() -> ! {
    unsafe {
        DEBUG_BREAK.write(0);
    }

    loop {}
}

#[panic_handler]
#[allow(clippy::empty_loop)]
fn core_panic(_info: &core::panic::PanicInfo) -> ! {
    defmt::error!("core::panic! called");

    defmt_panic()
}

pub fn ready() {
    DEBUG_INIT.write(0);
}
