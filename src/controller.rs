use crate::regs::cpu::JOY1;
use bitflags::bitflags;

bitflags! {
    pub struct Controller: u8 {
        const A = 0b10000000;
        const B = 0b01000000;
        const SELECT = 0b00100000;
        const START = 0b00010000;
        const UP = 0b00001000;
        const DOWN = 0b00000100;
        const LEFT = 0b00000010;
        const RIGHT = 0b00000001;
    }
}

impl Controller {
    pub fn read_joy1() -> Self {
        let mut byte = 0;

        JOY1.write(0x1);
        JOY1.write(0x0);

        for _ in 0..8 {
            byte <<= 1;
            byte |= JOY1.read() & 1;
        }

        Self::from_bits_truncate(byte)
    }
}
