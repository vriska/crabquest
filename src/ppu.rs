use crate::regs::{cpu::OAMDMA, ppu::*};

#[derive(Default, Copy, Clone)]
#[repr(C)]
pub struct Sprite {
    pub y: u8,
    pub tile: u8,
    pub attributes: u8,
    pub x: u8,
}

impl Sprite {
    pub const fn new() -> Self {
        Self {
            y: 0,
            tile: 0,
            attributes: 0,
            x: 0,
        }
    }
}

#[derive(Copy, Clone)]
#[repr(C, align(256))]
pub struct Oam {
    pub sprites: [Sprite; 64],
}

impl Oam {
    pub const fn new() -> Self {
        Self {
            sprites: [Sprite::new(); 64],
        }
    }

    // making this #[inline(never)] prevents dead store elimination
    #[inline(never)]
    pub fn write(&self) {
        let addr = self as *const _;
        let high_addr = (addr as usize >> 8) as u8;

        OAMADDR.write(0);
        unsafe {
            OAMDMA.write(high_addr);
        }
    }
}

impl Default for Oam {
    fn default() -> Self {
        Self::new()
    }
}

pub fn copy_to_ppu(src: &[u8], dst: u16) {
    PPUADDR.write((dst >> 8) as u8);
    PPUADDR.write((dst & 0xFF) as u8);
    for &byte in src {
        PPUDATA.write(byte);
    }
}

pub fn wait_for_vblank() {
    while (PPUSTATUS.read() & 0x80) == 0 {}
}

pub fn enable_rendering() {
    PPUMASK.write(0b00011110);
}

pub fn reset_ppuctrl() {
    // SAFETY: NMI handler already exists
    unsafe {
        PPUCTRL.write(0b10001000);
    }
}

pub fn set_scroll(x: u8, y: u8) {
    PPUSCROLL.write(x);
    PPUSCROLL.write(y);
    reset_ppuctrl();
}

pub fn clear_nmi() {
    PPUSTATUS.read();
}
