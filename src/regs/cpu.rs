use voladdress::*;

pub const SQ1_VOL: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4000) };
pub const SQ1_SWEEP: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4001) };
pub const SQ1_LO: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4002) };
pub const SQ1_HI: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4003) };

pub const SQ2_VOL: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4004) };
pub const SQ2_SWEEP: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4005) };
pub const SQ2_LO: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4006) };
pub const SQ2_HI: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4007) };

pub const TRI_LINEAR: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4008) };
pub const TRI_LO: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x400A) };
pub const TRI_HI: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x400B) };

pub const NOISE_VOL: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x400C) };
pub const NOISE_LO: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x400E) };
pub const NOISE_HI: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x400F) };

pub const DMC_FREQ: VolAddress<u8, (), Unsafe> = unsafe { VolAddress::new(0x4010) };
pub const DMC_RAW: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4011) };
pub const DMC_START: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4012) };
pub const DMC_LEN: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4013) };

pub const OAMDMA: VolAddress<u8, (), Unsafe> = unsafe { VolAddress::new(0x4014) };

pub const SND_CHN: VolAddress<u8, Safe, Safe> = unsafe { VolAddress::new(0x4015) };

pub const JOY1: VolAddress<u8, Safe, Safe> = unsafe { VolAddress::new(0x4016) };
pub const JOY2: VolAddress<u8, Safe, Safe> = unsafe { VolAddress::new(0x4017) };

// debug logging, normally open-bus
pub const DEBUG_WRITE: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4009) };
pub const DEBUG_FLUSH: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x400D) };
pub const DEBUG_BREAK: VolAddress<u8, (), Unsafe> = unsafe { VolAddress::new(0x4018) };
pub const DEBUG_INIT: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x4019) };
