use voladdress::*;

pub const PPUCTRL: VolAddress<u8, (), Unsafe> = unsafe { VolAddress::new(0x2000) };
pub const PPUMASK: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x2001) };
pub const PPUSTATUS: VolAddress<u8, Safe, ()> = unsafe { VolAddress::new(0x2002) };
pub const OAMADDR: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x2003) };
pub const OAMDATA: VolAddress<u8, Safe, Safe> = unsafe { VolAddress::new(0x2004) };
pub const PPUSCROLL: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x2005) };
pub const PPUADDR: VolAddress<u8, (), Safe> = unsafe { VolAddress::new(0x2006) };
pub const PPUDATA: VolAddress<u8, Safe, Safe> = unsafe { VolAddress::new(0x2007) };
