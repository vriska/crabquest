use crate::ppu::{self, *};
use core::cell::SyncUnsafeCell;
use core::sync::atomic::{AtomicBool, Ordering};
use tinyvec::ArrayVec;

static SYNC_QUEUE: SyncQueue = SyncQueue::new();

struct VramWrites {
    buf: ArrayVec<[u8; 200]>,
}

impl VramWrites {
    pub const fn new() -> Self {
        Self {
            buf: ArrayVec::from_array_empty([0; 200]),
        }
    }

    pub fn push(&mut self, data: &[u8], address: u16) {
        self.buf
            .extend_from_slice(&(data.len() as u16).to_le_bytes());
        self.buf.extend_from_slice(&address.to_le_bytes());
        self.buf.extend_from_slice(data);
    }

    pub fn apply(&mut self) {
        let mut idx = 0;

        while idx < self.buf.len() {
            let len = u16::from_le_bytes([self.buf[idx], self.buf[idx + 1]]) as usize;
            idx += 2;

            let address = u16::from_le_bytes([self.buf[idx], self.buf[idx + 1]]);
            idx += 2;

            let data = &self.buf[idx..][..len];

            ppu::copy_to_ppu(data, address);

            idx += len;
        }

        self.buf.clear();
    }
}

struct QueuedOps {
    pub oam: Oam,
    pub writes: VramWrites,
    pub scroll: (u8, u8),
}

impl QueuedOps {
    pub const fn new() -> Self {
        Self {
            oam: Oam::new(),
            writes: VramWrites::new(),
            scroll: (0, 0),
        }
    }

    pub fn apply(&mut self) {
        self.writes.apply();
        self.oam.write();
        ppu::set_scroll(self.scroll.0, self.scroll.1);
    }
}

struct SyncQueue {
    ready: AtomicBool,
    data: SyncUnsafeCell<QueuedOps>,
}

impl SyncQueue {
    pub const fn new() -> Self {
        Self {
            ready: AtomicBool::new(false),
            data: SyncUnsafeCell::new(QueuedOps::new()),
        }
    }

    pub fn read(&self) -> Option<&mut QueuedOps> {
        let ready = self.ready.load(Ordering::Acquire);
        if ready {
            self.ready.store(false, Ordering::Release);
            // SAFETY: if ready is set, other thread must be blocking inside mark_ready
            Some(unsafe { &mut *self.data.get() })
        } else {
            None
        }
    }

    /// # Safety
    /// Ensure there are no active references to QueuedOps before calling this function.
    #[allow(clippy::mut_from_ref)]
    pub unsafe fn write(&self) -> &mut QueuedOps {
        &mut *self.data.get()
    }

    pub fn mark_ready(&self) {
        self.ready.store(true, Ordering::Release);
        while self.ready.load(Ordering::Acquire) {
            core::hint::spin_loop();
        }
    }
}

/// # Safety
/// Should only be called by the system.
#[no_mangle]
pub unsafe extern "mos-non-recursive-interrupt" fn nmi() {
    if let Some(ops) = SYNC_QUEUE.read() {
        ops.apply();
    }

    clear_nmi();
}

pub fn end_frame() {
    SYNC_QUEUE.mark_ready();
}

pub fn copy_to_ppu(data: &[u8], address: u16) {
    let queue = unsafe { SYNC_QUEUE.write() }; // SAFETY: not reentrant
    queue.writes.push(data, address);
}

pub fn update_sprite(idx: u8, new: Sprite) {
    let queue = unsafe { SYNC_QUEUE.write() }; // SAFETY: not reentrant
    queue.oam.sprites[usize::from(idx)] = new;
}

pub fn set_scroll(x: u8, y: u8) {
    let queue = unsafe { SYNC_QUEUE.write() }; // SAFETY: not reentrant
    queue.scroll = (x, y);
}
