#![feature(used_with_arg, sync_unsafe_cell)]
#![no_std]
#![no_main]

use controller::Controller;
use nmi::*;
use ppu::Sprite;

pub mod controller;
pub mod log;
pub mod nmi;
pub mod ppu;
pub mod regs;

const TEST_CHR0: [u8; 0x1000] = *include_bytes!("test_chr0.bin");
const TEST_CHR1: [u8; 0x1000] = *include_bytes!("test_chr1.bin");

#[no_mangle]
#[used(linker)]
#[link_section = ".chr_rom"]
pub static CHR_ROM: [u8; 0x2000] = {
    let mut arr = [0; 0x2000];
    let mut i = 0;
    while i < 0x1000 {
        arr[i] = TEST_CHR0[i];
        arr[i + 0x1000] = TEST_CHR1[i];
        i += 1;
    }
    arr
};

static NAMETABLE: [u8; 0x400] = *include_bytes!("test_nametable.bin");

#[no_mangle]
pub extern "C" fn main() {
    log::ready();

    defmt::info!("Hello, world!");

    ppu::wait_for_vblank();

    ppu::copy_to_ppu(&NAMETABLE, 0x2000);
    ppu::copy_to_ppu(&[0x10; 0x20], 0x3F00);

    ppu::enable_rendering();
    ppu::reset_ppuctrl();

    let mut player = Sprite {
        x: 50,
        y: 50,
        attributes: 0b00000011,
        tile: 1,
    };

    let mut frames = 0u8;
    let mut color = 0u8;

    loop {
        frames += 1;

        if frames == 30 {
            color = color.overflowing_add(1).0;

            copy_to_ppu(&[color], 0x3f00);

            frames = 0;

            defmt::debug!("new color: {:02x}", color);
        }

        let controller = Controller::read_joy1();

        if controller.contains(Controller::UP) {
            player.y -= 1;
        } else if controller.contains(Controller::DOWN) {
            player.y += 1;
        }

        if controller.contains(Controller::LEFT) {
            player.x -= 1;
        } else if controller.contains(Controller::RIGHT) {
            player.x += 1;
        }

        if controller
            .intersects(Controller::UP | Controller::DOWN | Controller::LEFT | Controller::RIGHT)
        {
            defmt::trace!("new position: {},{}", player.x, player.y);
        }

        update_sprite(0, player);

        end_frame();
    }
}
